import { argv } from 'node:process'
import fs from 'fs/promises'
import { getExtendedFlightData, toCsv } from '../flightTracker'
import type { ArrivalOrDepart } from '../openSky'

const [_, __, user, pass, arrOrDepart, airport, startArg, endArg, filename] = argv

if (!filename || !['arrival', 'departure', 'all'].includes(arrOrDepart) || airport.length !== 4) {
  throw new Error(
    "Incorrect arguments, needs 'user pass arrival/departure airport-icao start end filename'"
    + ` got ${argv.slice(2).join(', ')}`
  )
}
const start = Date.parse(startArg)
const end = Date.parse(endArg)
if (Number.isNaN(start) || Number.isNaN(end)) {
  throw new Error(`Invalid date arguments, try the form YYYY-mm-dd hh:mm:ss, start: ${start}, end: ${end}`)
}

getExtendedFlightData({
  apiUser: user,
  apiPass: pass,
  arrOrDepart: arrOrDepart as ArrivalOrDepart,
  airport,
  start,
  end,
})
  .then((flights) => {
    console.log('All done. Writing out...')
    const csv = toCsv(flights.map((flight) => ({ ...flight, start: new Date(flight.start), end: new Date(flight.end) })))
    return fs.writeFile(filename, csv)
  })
  .catch(console.error)
