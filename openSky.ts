import axios from 'axios'

export type ArrivalOrDepart = 'arrival' | 'departure' | 'all'

export interface Flight {
  callsign: string
  arrivalAirportCandidatesCount: number
  estArrivalAirport: string | null
  estArrivalAirportHorizDistance: number | null
  estArrivalAirportVertDistance: number | null
  departureAirportCandidatesCount: number
  estDepartureAirport: string | null
  estDepartureAirportHorizDistance: number | null
  estDepartureAirportVertDistance: number | null
  firstSeen: number
  icao24: string
  lastSeen: number
}

export interface Aircraft {
  icao24: string
  registration: string
  model: string
  operator: string
  country: string
}

export async function getFlights({
  airport,
  apiPass,
  apiUser,
  arrOrDepart,
  end,
  start,
}: {
  airport: string
  apiPass: string
  apiUser: string
  arrOrDepart: ArrivalOrDepart
  end: number
  start: number
}) {
  const res = await axios.get<Flight[]>(`https://opensky-network.org/api/flights/${arrOrDepart}`, {
    params: {
      ...arrOrDepart === 'all' ? {} : { airport },
      begin: start / 1000,
      end: end / 1000,
    },
    auth: { username: apiUser, password: apiPass },
  })
  return res.data
}

interface AircraftListResponse {
  content: Aircraft[]
  totalPages: number
  totalElements: number
  first: boolean
  last: boolean
  sort: string
  numberOfElements: number
  size: number
  number: number
}
export async function lookUpAircraftList({
  icao24,
}: {
  icao24: string
}) {
  const res = await axios.get<AircraftListResponse>(
    'https://opensky-network.org/api/metadata/aircraft/list',
    { params: { q: icao24, n: 5, p: 1, sc: '', sd: '' } }
  )
  return res.data.content.find((plane) => plane.icao24 === icao24)
}

interface AircraftResponse {
  registration: string
  manufacturerName: string
  manufacturerIcao: string
  model: string
  typecode: string
  serialNumber: string
  lineNumber: string
  icaoAircraftClass: string
  selCal: string
  operator: string
  operatorCallsign: string
  operatorIcao: string
  operatorIata: string
  owner: string
  categoryDescription: string
  registered: string | null
  regUntil: string
  status: string
  built: string
  firstFlightDate: string | null
  engines: string
  modes: boolean
  adsb: boolean
  acars: boolean
  vdl: boolean
  notes: string
  country: string
  lastSeen: number | null
  firstSeen: number | null
  icao24: string
  timestamp: number
}
export async function lookUpAircraft({
  icao24,
}: {
  icao24: string
}) {
  try {
    const res = await axios.get<AircraftResponse>(
      `https://opensky-network.org/api/metadata/aircraft/icao/${icao24}`
    )
    return res.data
  } catch (err) {
    if (err && typeof err === 'object' && 'status' in err && err.status !== 404) {
      console.error('unknown error', icao24, err)
    }
  }
}
