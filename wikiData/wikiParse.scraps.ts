// Attempt to loop through pages or tokens...
// function forEachChild<T extends Token | Pages>(token: T, cb: (child: T extends Pages ? Page : TokenChild) => void) {
// function forEachChild(token: Token, cb: (child: TokenChild) => void): void;
// function forEachChild<T extends Token | Pages>(token: T, cb: (child: T extends Pages ? Page : TokenChild) => void) {
// // function forEachChild(token: Token | Pages, cb: (child: Page | TokenChild) => void): void {
//   for (let i = 0; !!token[`${i}`]; i++) {
//     const child = token[`${i}`]
//     if (isPages(token)) {
//       const pchild = token[`${i}`]
//       pchild && cb(pchild)
//     } else if (typeof child === 'string' || (typeof child === 'object' && child.type)) {
//       cb(child)
//     } else {
//       console.warn('fail', child)
//     }
//   }
// }

// This doesn't work as it doesn't actually fetch the pages...
// const isPages = (something: Token | Pages): something is Pages => "index_of_title" in something
// function forEachPage(pages: Pages, cb: (page: Page) => void): void {
//   for (let i = 0; !!pages[`${i}`]; i++) {
//     const child = pages[`${i}`]
//     if (isPages(pages)) {
//       child && cb(child)
//     } else {
//       console.warn('Not pages?', Object.keys(pages), pages)
//     }
//   }
// }
