import fs from 'fs'
import { getAirlineData } from './wikiParse'

getAirlineData()
  .then((data) => {
    [...data.keys()].filter((icao) => icao?.length !== 3)
      .forEach((icao) => console.warn('incorrect format:', icao))
    const dataObject = Object.fromEntries(data.entries())
    fs.writeFileSync('./data/wiki-airlines.json', JSON.stringify(dataObject, undefined, 2))
  })
  .catch(console.error)
