///<reference path="wikiapi.d.ts"/>

import wikiapi, {
  LinkToken,
  TableCellToken,
  TableRowToken,
  Token,
  TokenChild,
} from 'wikiapi'

export interface Airline {
  iata: string
  icao: string
  name: string
  country: string
}
interface ColumnMapping {
  wiki: string
  js: keyof Airline
}
const columnMapping: ColumnMapping[] = [
  { wiki: 'IATA', js: 'iata' },
  { wiki: 'ICAO', js: 'icao' },
  { wiki: 'Airline', js: 'name' },
  { wiki: 'Country', js: 'country' },
]

const isTableRow = (token: Token): token is TableRowToken =>
  token.type === 'table_row' || (token.type === 'caption'
    && typeof token.header_count === 'number'
    && token.header_count > 3)
const isTableCell = (token: Token): token is TableCellToken => token.type === 'table_cell'
const isLinkToken = (token: Token): token is LinkToken => token.type === 'link'

function forEachChild(token: Token, cb: (child: TokenChild) => void): void {
  for (let i = 0; !!token[`${i}`]; i++) {
    const child = token[`${i}`]
    if (typeof child === 'string' || (typeof child === 'object' && child.type)) {
      cb(child)
    } else {
      console.warn('Incorrect type', child, token)
    }
  }
}

function extractTableCellContents(token: Token): string {
  const cellContent = token['0']
  if (typeof cellContent === 'string') {
    return cellContent.trim()
  } else if (typeof cellContent !== 'object' || !cellContent.type) {
    throw new Error(`Unknown content type in table cell ${cellContent}`)
  } else if (isLinkToken(cellContent)) {
    if (cellContent['0'].type !== 'namespaced_title') {
      console.error('Data structure doesn\'t match but it might still work', token)
    }
    return cellContent['0']['0']
  } else {
    if (!['italic', 'tag_attributes', 'tag', 'tag_single', 'table_attributes']
        .includes(cellContent.type)) {
      console.warn('Recursing nested content, hopefully this works', cellContent.type, token)
    }
    return extractTableCellContents(cellContent)
  }
}
function extractTableCells(token: TableRowToken): string[] {
  let cells: string[] = []
  forEachChild(token, (cur) => {
    if (typeof cur === 'object' && isTableCell(cur)) {
      cells.push(extractTableCellContents(cur))
    }
  })
  return cells
}

async function getPageOfAirlineData(wiki: wikiapi, pageName: string) {
  const page = await wiki.page(pageName)
  const dataIndicesPartial: Partial<Record<keyof Airline, number>> = {}
  let dataIndices: Record<keyof Airline, number> | undefined
  const airlines = new Map<string, Airline>()
  page.parse().each((token) => {
    if (isTableRow(token)) {
      if (token.header_count >= 1) {
        const headers: string[] = []
        forEachChild(token, (rowChild) => {
          if (typeof rowChild === 'object' && isTableCell(rowChild) && rowChild.is_header) {
            headers.push(extractTableCellContents(rowChild))
          }
        })
        // Find header order and verify content
        columnMapping.forEach(({ wiki, js }) => {
          const index = headers.indexOf(wiki)
          if (!(index >= 0)) throw new Error(`Unable to find ${wiki} column in table`)
          dataIndicesPartial[js] = index
        })
        dataIndices = dataIndicesPartial as typeof dataIndices
      } else {
        const cells = extractTableCells(token)
        if (!dataIndices) {
          console.error('No table header', page, token)
          throw new Error(`Table header doesn't seem to exist for row containing ${cells[1]}`)
        }
        const airline: Airline = {
          iata: cells[dataIndices['iata']],
          icao: cells[dataIndices['icao']],
          name: cells[dataIndices['name']],
          country: cells[dataIndices['country']],
        }
        airlines.set(airline.icao, airline)
      }
    }
  })

  return airlines
}

const allPageNames = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K',
    'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '0-9']
  .map((letter) => `List of airline codes (${letter})`)

export async function getAirlineData() {
  const wiki = new wikiapi('en')
  const pagedData: Map<string, Airline>[] = []
  for (const pageName of allPageNames) {
    console.log('reading page', pageName)
    pagedData.push(await getPageOfAirlineData(wiki, pageName))
  }
  const allData = new Map<string, Airline>()
  pagedData.forEach((page) => page.forEach((value, key) => allData.set(key, value)))
  allData.delete('')

  return allData
}

// MAYBE: add a test which checks for a couple of airlines from each to confirm all letters are included?
