declare module 'wikiapi' {
  export type TokenChild = Token | string
  export interface Token {
    type: string
    toString: () => string
    // There's no obvious way to limit this to number strings, so we need to include
    // any possible sub type
    [index: string]: TokenChild | number | boolean | undefined
  }
  export interface FormattingToken extends Token {
    // TODO add more if there are problems
    type: 'italic'
    ['0']: Token
    ['1']: string
  }
  export interface NamespacedTitleToken extends Token {
    type: 'namespaced_title'
    ['0']: string
  }
  export interface LinkToken extends Token {
    type: 'link'
    is_link: true
    anchor: string
    ['0']: NamespacedTitleToken
    ['1']: string
  }
  export interface TableCellToken extends Token {
    type: 'table_cell'
    delimiter: string
    is_header: boolean
    ['0']: TokenChild
  }
  export interface TableRowToken extends Token {
    type: 'table_row' | 'caption'
    delimiter: string
    header_count: number
    data_count: number
    [index: string]: Token
  }
  export interface ParsedPage {
    each: (it: (token: Token) => void) => void
  }
  export interface Page {
    parse: () => ParsedPage
  }
  export interface Pages {
    index_of_title: Record<string, number>
    [index: string]: Page | undefined
  }
  export default class WikiApi {
    constructor(site: string)
    page(title: string): Promise<Page>
  }
}

