// @ts-ignore
import { Parser as JsonToCsv } from '@json2csv/plainjs'
import airportsIn from './airports/airports.json'
import airlinesIn from './data/wiki-airlines.json'
import airlineClass from './data/airlineClass.json'
import fuelUsageIn from './data/fuelUsage.json'
import { ArrivalOrDepart, Flight, getFlights, lookUpAircraft } from './openSky'
import type { Airline } from './wikiData/wikiParse'

type FlightType = 'Chartered' | 'Likely chartered' | 'Scheduled' | 'Unknown'
interface Tracked {
  start: number
  end: number
  duration: number
  icao24: string
  callsign: string
  arrivalAirport: string
  departureAirport: string
  airline: string
  flightType: FlightType
}

interface Airport {
  name?: string
  country: string
}
const airports: Record<string, Airport> = airportsIn as any
const airlines: Record<string, Airline> = airlinesIn as any
const fuelUsage: Record<string, number | undefined> = fuelUsageIn as any
const scheduledAirlines = new Set(airlineClass.scheduled)
const charteredAirlines = new Set(airlineClass.charter)

const toCsvDate = (date: Date) =>
  `${date.getUTCFullYear()}-${date.getUTCMonth()}-${date.getUTCDate()} ${
    date.getUTCHours()}:${date.getUTCMinutes()}:${date.getUTCSeconds()}`

const jsonToCsv = new JsonToCsv({
  formatters: {
    object: (item: object) => item instanceof Date ? toCsvDate(item) : '',
  }
})
export function toCsv(data: object[]): string {
  if (data.length === 0) return '<no data>'
  return jsonToCsv.parse(data)
}

export function processData(fType: ArrivalOrDepart, flights: Flight[]): Tracked[] {
  return flights.map((flight) => {
    const arrAirportCode = flight.estArrivalAirport
    const arrivalAirport = arrAirportCode ? airports[arrAirportCode] : null
    const depAirportCode = flight.estDepartureAirport
    const departureAirport = depAirportCode ? airports[depAirportCode] : null
    const possibleAirlineCode = flight.callsign.slice(0, 3)
    const airline = airlines[possibleAirlineCode]?.name ?? ''

    let flightType: FlightType
    if (!airline) {
      // If we don't have an airline it's likely a charter flight
      flightType = 'Likely chartered'
    // Then look at airline
    } else if (scheduledAirlines.has(possibleAirlineCode)) {
      flightType = 'Scheduled'
    } else if (charteredAirlines.has(possibleAirlineCode)) {
      flightType = 'Chartered'
    } else {
      flightType = 'Unknown'
    }
    
    return {
      start: flight.firstSeen * 1000,
      end: flight.lastSeen * 1000,
      duration: (flight.lastSeen - flight.firstSeen) / 60,
      icao24: flight.icao24,
      callsign: flight.callsign?.trim(),
      arrivalAirport: arrivalAirport?.name ?? '',
      departureAirport: departureAirport?.name ?? '',
      airline,
      flightType,
    }
  })
}

const wait = async (time: number) => new Promise((resolve) => setTimeout(resolve, time))

export async function getExtendedFlightData(getFlightsArgs: Parameters<typeof getFlights>[0]) {
  const flights = await getFlights(getFlightsArgs)
  const processed = processData(getFlightsArgs.arrOrDepart, flights)
  const allData = []
  console.log(`Got ${flights.length} flights, looking up aircraft data...`)
  for (const flight of processed) {
      let craft
      try {
        // Do this slowly to avoid overloading API
        await wait(200)
        craft = await lookUpAircraft({ icao24: flight.icao24 })
      } catch (err) {
        console.error('Error getting extra data', err)
      }
      if (craft) {
        let litres = ''
        let co2 = ''
        if (craft.typecode && flight.duration) {
          const galsPerHour = fuelUsage[craft.typecode]
          if (galsPerHour) {
            const litresNum = ( galsPerHour * 3.78541 / 60 ) * flight.duration
            litres = `${litresNum}`
            co2 = `${litresNum * 3.06}`
          }
        }
        allData.push({
          ...flight,
          registration: craft.registration,
          litres,
          co2,
          manufacturerName: craft.manufacturerName,
          manufacturerIcao: craft.manufacturerIcao,
          model: craft.model,
          typecode: craft.typecode,
          owner: craft.owner,
          country: craft.country,
        })
      } else {
        allData.push(flight)
      }
  }

  return allData
}
